#include <zephyr/kernel.h>
#include <lte_client/lte_client.h>
#include <sen5x/sen5x.h>
#include <stdio.h>
#include <stdlib.h>

void main(void)
{
	
	printk("Welcome to IOTPM running on %s\n", CONFIG_BOARD);
	
	printk("Start the Sen55\n");
	sen5x_init(DEVICE_DT_GET(DT_NODELABEL(i2c2)));
	sen5x_reset();
	
	printk("Get the sensor information\n");
	struct sen5x_info sen;
	sen5x_get_info(&sen);
	sen5x_print_info(&sen);
	
	printk("Connect to the LTE\n");
	if(!lte_client_connect()){
		struct lte_client_system_mode mode;
		mode.lte_m=0; mode.nb_iot=1; mode.gnss=1; mode.lte_p=0;
		lte_client_set_system_mode(&mode);
		lte_client_connect();
	}
	
	printk("Get the network information\n");
	struct lte_client_net_info net;
	lte_client_init_net_info(&net);
	lte_client_get_net_info(&net);
	lte_client_print_net_info(&net);	
	
	printk("Get the gps coordinates\n");
	struct lte_client_gps_info gps;
	lte_client_init_gps_info(&gps);
	lte_client_get_gps_info(&gps);
	
	struct sen5x_measurement pmm;

	while (true) {

		printk("Start measurement\n");
		sen5x_start();

		k_msleep(1*60000);

		sen5x_get_measurement(&pmm);
		sen5x_print_measurement(&pmm);
		
		char payload[300];
		char * str=payload;
		str += sprintf(str, "cmd=add_sen55");
		str += sprintf(str, "&sensor_id=%15s",net.imei);
		str += sprintf(str, "&area=%s",net.area);
		str += sprintf(str, "&operator=%s",net.operator);
		str += sprintf(str, "&cellid=%s",net.cellid);
		if(gps.valid){
			str += sprintf(str, "&lat=%s",gps.latitude_string);
			str += sprintf(str, "&lon=%s",gps.longitude_string);
			str += sprintf(str, "&alt=%s",gps.altitude_string);
		}
		str += sprintf(str, "&temp=%i.%02i",(pmm.ambient_temperature / 200),abs(pmm.ambient_temperature % 200)>>1);
		str += sprintf(str, "&humi=%i.%02i",(pmm.ambient_humidity / 100),(pmm.ambient_humidity % 100));
		str += sprintf(str, "&pm1p0=%i.%02i",(pmm.mass_concentration_pm1p0 / 10),(pmm.mass_concentration_pm1p0 % 10));
		str += sprintf(str, "&pm2p5=%i.%02i",(pmm.mass_concentration_pm2p5 / 10),(pmm.mass_concentration_pm2p5 % 10));
		str += sprintf(str, "&pm4p0=%i.%02i",(pmm.mass_concentration_pm4p0 / 10),(pmm.mass_concentration_pm4p0 % 10));
		str += sprintf(str, "&pm10p0=%i.%02i",(pmm.mass_concentration_pm10p0 / 10),(pmm.mass_concentration_pm10p0 % 10));
		str += sprintf(str, "&voc=%i.%02i",(pmm.voc_index / 10),abs(pmm.voc_index % 10));
		str += sprintf(str, "&nox=%i.%02i",(pmm.nox_index / 10),abs(pmm.nox_index % 10));
		printk("payload: %s\n",payload);
		
		lte_client_post("https","sacaqm.web.cern.ch","443","/dbwrite.php",payload);
	
		printk("Stop measurement\n");
		sen5x_stop();
	
		if(!gps.valid){
			lte_client_get_gps_info(&gps);
		}else{
			printk("Sleep\n");
			k_msleep(4*60000);
		}
	
	}

}

	