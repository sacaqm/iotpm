/** 
 * LTE client library for Nordic nRF9160DK
 * based on the LTE_LC, MODEM_INFO, and GNSS_INTERFACE libraries
 *
 * https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/libraries/modem/lte_lc.html
 * https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/libraries/modem/modem_info.html
 * https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrfxlib/nrf_modem/doc/gnss_interface.html
 *
 * Carlos.Solans_AT_CERN
 * February 2023
 */

#include <stdint.h>

/**
 * @brief NRF9160DK system mode structure
 * Structure contains if LTE-M, NB-IOT, GNSS should be enabled
 * And the preference for connection
 * 0=automatic
 * 1=prefer LTE-M over network selection
 * 2=prefer NB-IOT over network selection
 * 3=prefer stay on home network and use NB-IOT if LTE-M is not available
 * 4=prefer stay on home network and use LTE-M if NB-IOT is not available
 */
struct lte_client_system_mode{
	int lte_m;
	int nb_iot;
	int gnss;
	int lte_p; 
};

/**
 * @brief LTE network information structure
 * Structure contains the IMEI, ICCID, APN, IP, AREA, OPERATOR, and CELL ID
 *
 */
struct lte_client_net_info{
	char imei[20]; 
	char iccid[30];
	char apn[80];
	char ip[20];
	char area[10]; 
	char operator[10];
	char cellid[10];
	char signalstrengh[10];
	char uemode[10];
	char gps[10];
	char mcc[10];
	char mnc[10];
};

/**
 * @brief GPS information structure
 * Structure that contains the latitude, longitude, and altitude
 * as floating point numbers and as strings
 * of the current GPS coordinates provided by lte_client_get_gps
 */
struct lte_client_gps_info{
	double latitude;
	double longitude;
	double altitude;
	char latitude_string[20];
	char longitude_string[20];
	char altitude_string[20];
	bool valid;
};

/**
 * @brief Set the system mode for the NRF9160DK to desired settings
 * @param mode pointer to a struct lte_client_system_mode with the configuration
 */
void lte_client_set_system_mode(struct lte_client_system_mode * mode);

/**
 * @brief Get the system mode for the NRF9160DK to desired settings
 * @param mode pointer to a struct lte_client_system_mode where to store the configuration
 */
void lte_client_get_system_mode(struct lte_client_system_mode * mode);

/**
 * @brief Connect to the LTE network
 * @return true if it connected succesfully
 */
bool lte_client_connect();

/**
 * @brief Initialize the network information struct
 * @param info pointer to a struct lte_client_net_info to initialize
 */
void lte_client_init_net_info(struct lte_client_net_info * info);

/**
 * @brief Get the network information from the underlaying LTE MODEM_INFO
 * @param info pointer to a struct lte_client_net_info to store the result
 */
void lte_client_get_net_info(struct lte_client_net_info * info);

/**
 * @brief Print the network information from the underlaying LTE MODEM_INFO
 * @param info pointer to a struct lte_client_net_info with the values to print
 */
void lte_client_print_net_info(struct lte_client_net_info * info);

/**
 * @brief Initialize the GPS location structure to zero
 * @param info pointer to a struct lte_client_gps_info to store the result
 */
void lte_client_init_gps_info(struct lte_client_gps_info * info);

/**
 * @brief Get the GPS location from the underlaying GNSS interface
 * This requires the position information from at least 12 satellites.
 * Function takes at most 3 minutes to complete.
 * Every 60 seconds it will check if the GPS location has been fixed, 
 * and try up to 3 times to get it.
 * @param info pointer to a struct lte_client_gps_info to store the result
 */
void lte_client_get_gps_info(struct lte_client_gps_info * info);

/**
 * @brief Test method to get triangulation from several LTE towers
 * This takes 40 seconds
 */
void lte_client_get_triangulation_info();

/**
 * @brief Send an HTTP/HTTPS request to a server identified by a schema, host, port, url and payload
 * @param schema protocol to use for communication: only values http or https are accepted. Default is http. 
 * @param host name of the host where the server is running
 * @param port number of the port where the server is listening to
 * @param url web page to access
 * @param payload query string containing the request data payload
 */
void lte_client_post(const char * schema, const char * host, const char * port, const char * url, const char * payload);

/**
 * @brief Enable/Disable power saving mode
 * Required in order to get a GPS location
 * @param enable true to enable power saving mode, false to disable
 */ 
void lte_client_power_saving(bool enable);

